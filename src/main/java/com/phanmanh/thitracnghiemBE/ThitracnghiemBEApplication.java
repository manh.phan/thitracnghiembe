package com.phanmanh.thitracnghiemBE;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThitracnghiemBEApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThitracnghiemBEApplication.class, args);
    }

}
