package com.phanmanh.thitracnghiemBE.api;

import com.phanmanh.thitracnghiemBE.model.Questions;
import com.phanmanh.thitracnghiemBE.service.QuestionsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController @RequiredArgsConstructor
@RequestMapping("/api")
public class QuestionsAPI {
    private final QuestionsService questionsService;
    @GetMapping("/questions")
    public ResponseEntity<List<Questions>> getQuestions(){
        return ResponseEntity.ok().body(questionsService.getQuestions());
    }
    @GetMapping("/question/{id}")
    public ResponseEntity<Optional<Questions>> getQuestion(@PathVariable UUID id){
        return ResponseEntity.ok().body(questionsService.getQuestion(id));
    }
    @PostMapping("/question/save")
    public ResponseEntity<Questions> saveQuestions(@RequestBody Questions questions) {
        System.out.println(questions);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/question/save").toUriString());
        return ResponseEntity.created(uri).body(questionsService.saveQuestions(questions));
    }
    @PostMapping("/question/saveAll")
    public ResponseEntity<Optional<List<Questions>>> saveQuestionsAll(@RequestBody List<Questions> questions) {
        System.out.println(questions);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/question/saveAll").toUriString());
        return ResponseEntity.created(uri).body(questionsService.saveQuestionsAll(questions));
    }
    @PostMapping("/question/addToSubject")
    public ResponseEntity<Optional<Questions>> saveQuestionToSubject(@RequestBody Questions questions) {
        System.out.println(questions);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/question/addToSubject").toUriString());
        return ResponseEntity.created(uri).body(questionsService.saveQuestionToSubject(questions));
    }
    @PutMapping("/question/update")
    public ResponseEntity<Optional<Questions>> updateQuestion(@RequestBody Questions questions) {
        System.out.println(questions);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/question/update").toUriString());
        return ResponseEntity.created(uri).body(questionsService.updateQuestion(questions));
    }
    @DeleteMapping("/questions/delete/{id}")
    public ResponseEntity<?> delQuestion(@PathVariable UUID id){
        questionsService.delQuestion(id);
        return ResponseEntity.noContent().build();
    }
}