package com.phanmanh.thitracnghiemBE.api;

import com.phanmanh.thitracnghiemBE.model.Score;
import com.phanmanh.thitracnghiemBE.service.ScoreServe;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController @RequiredArgsConstructor
@RequestMapping("/api")
public class ScoreAPI {
    private final ScoreServe scoreServe;
    @GetMapping("/scores")
    public ResponseEntity<Optional<List<Score>>> getScore(){
        return ResponseEntity.ok().body(scoreServe.getScore());
    }
    @PostMapping("/score/save")
    public ResponseEntity<Optional<Score>> saveScore(@RequestBody Score score){
        return ResponseEntity.ok().body(scoreServe.saveScore(score));
    }
    @PutMapping("/score/update")
    public ResponseEntity<Optional<Score>> updateScore(@RequestBody Score score){
        return ResponseEntity.ok().body(scoreServe.updateScore(score));
    }
    @DeleteMapping("/score/delete/{id}")
    public ResponseEntity<?> deleteScore(@PathVariable UUID id){
        scoreServe.deleteScore(id);
        return ResponseEntity.noContent().build();
    }
}
