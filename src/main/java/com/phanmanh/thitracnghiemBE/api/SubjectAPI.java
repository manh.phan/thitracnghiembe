package com.phanmanh.thitracnghiemBE.api;

import com.phanmanh.thitracnghiemBE.model.Subject;
import com.phanmanh.thitracnghiemBE.service.SubjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController @RequiredArgsConstructor
@RequestMapping("/api")
public class SubjectAPI {
    private final SubjectService subjectService;
    @GetMapping("/subjects")
    public ResponseEntity<List<Subject>> getSubjects(){
        return ResponseEntity.ok().body(subjectService.getSubjects());
    }
    @GetMapping("/subject/{id}")
    public ResponseEntity<Optional<Subject>> getSubjects(@PathVariable UUID id){
        return ResponseEntity.ok().body(subjectService.getSubject(id));
    }
    @PostMapping("/subject/save")
    public ResponseEntity<Subject> saveSubject(@RequestBody Subject subject) {
        System.out.println(subject);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/subject/save").toUriString());
        return ResponseEntity.created(uri).body(subjectService.saveSubject(subject));
    }
    @PutMapping("/subject/update")
    public ResponseEntity<Optional<Subject>> updateSubject(@RequestBody Subject subject) {
        System.out.println(subject);
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/subject/update").toUriString());
        return ResponseEntity.created(uri).body(subjectService.updateSubject(subject));
    }
    @DeleteMapping("/subject/delete/{id}")
    public ResponseEntity<?> delSubject(@PathVariable UUID id){
        subjectService.delSubject(id);
        return ResponseEntity.noContent().build();
    }
}
