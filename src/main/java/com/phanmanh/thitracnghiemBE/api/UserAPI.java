package com.phanmanh.thitracnghiemBE.api;

import com.phanmanh.thitracnghiemBE.model.User;
import com.phanmanh.thitracnghiemBE.service.UserService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController @RequiredArgsConstructor
@RequestMapping("/api")
public class UserAPI {
    private final UserService userService;
    @GetMapping("/users")
    public ResponseEntity<List<User>> getUsers() {
        return ResponseEntity.ok().body(userService.getUsers());
    }
    @GetMapping("/user/{id}")
    public ResponseEntity<Optional<User>> getUser(@PathVariable UUID id) {
        return ResponseEntity.ok().body(userService.getUser(id));
    }
    @PutMapping("user/update")
    public ResponseEntity<Optional<User>> updateUser(@RequestBody User user) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/user/update").toUriString());
        return ResponseEntity.created(uri).body(userService.updateUser(user));
    }
    @PostMapping("user/save")
    public ResponseEntity<Optional<User>> saveUser(@RequestBody User user) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/user/save").toUriString());
        return ResponseEntity.created(uri).body(userService.saveUser(user));
    }
    @DeleteMapping("user/delete/{id}")
    public ResponseEntity<?> delUser(@PathVariable UUID id) {
        userService.delUser(id);
        return ResponseEntity.noContent().build();
    }
    @PostMapping("user/addToRole")
    public ResponseEntity<Optional<User>> saveUserToRole(@RequestBody UserToRoleForm userToRoleForm ) {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("api/user/addToRole").toUriString());
        Optional<User> user = userService.saveUserToRole(userToRoleForm.getUserID(), userToRoleForm.getRoleID());
        return ResponseEntity.created(uri).body(user);
    }
}

@Data
class UserToRoleForm {
    private UUID userID;
    private UUID roleID;
}
