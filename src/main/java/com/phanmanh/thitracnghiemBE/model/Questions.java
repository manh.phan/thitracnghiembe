package com.phanmanh.thitracnghiemBE.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
public class Questions {
    @Id
    @GeneratedValue( generator = "uuid2" )
    @GenericGenerator( name = "uuid2", strategy = "uuid2" )
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    private String question;
    private String answers1;
    private String answers2;
    private String answers3;
    private String answers4;
    private int correctIndex;
    @ManyToOne
    private Subject subject;
}