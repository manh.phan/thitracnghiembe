package com.phanmanh.thitracnghiemBE.model;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
public class Score {
    @Id
    @GeneratedValue( generator = "uuid2" )
    @GenericGenerator( name = "uuid2", strategy = "uuid2" )
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Subject subject;

    private Double score;
}
