package com.phanmanh.thitracnghiemBE.repository;

import com.phanmanh.thitracnghiemBE.model.Questions;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface QuestionsRepository extends JpaRepository<Questions, UUID> {
    Optional<Questions> findById(UUID id);
}