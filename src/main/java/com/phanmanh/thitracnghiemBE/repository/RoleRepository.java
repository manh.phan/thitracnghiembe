package com.phanmanh.thitracnghiemBE.repository;

import com.phanmanh.thitracnghiemBE.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Optional<Role> findById(UUID id);
}