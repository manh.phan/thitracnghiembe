package com.phanmanh.thitracnghiemBE.repository;

import com.phanmanh.thitracnghiemBE.model.Score;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ScoreRepository extends JpaRepository<Score, UUID> {
}