package com.phanmanh.thitracnghiemBE.repository;

import com.phanmanh.thitracnghiemBE.model.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface SubjectRepository extends JpaRepository<Subject, UUID> {
    Optional<Subject> findById(UUID id);
}