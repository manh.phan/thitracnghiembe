package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Questions;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface QuestionsService {

    List<Questions> getQuestions();
    Optional<Questions> getQuestion(UUID id);
    Optional<Questions> saveQuestionToSubject(Questions questions);
    Questions saveQuestions(Questions questions);
    Optional<List<Questions>> saveQuestionsAll(List<Questions> questionsList);
    Optional<Questions> updateQuestion(Questions questions);
    void delQuestion(UUID id);
}
