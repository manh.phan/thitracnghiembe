package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Questions;
import com.phanmanh.thitracnghiemBE.repository.QuestionsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service @RequiredArgsConstructor
public class QuestionsServiceImp implements QuestionsService{
    private final QuestionsRepository questionsRepository;

    @Override
    public List<Questions> getQuestions() {
        return questionsRepository.findAll();
    }

    @Override
    public Optional<Questions> getQuestion(UUID id) {
        return questionsRepository.findById(id);
    }

    @Override
    public Optional<Questions> saveQuestionToSubject(Questions questions) {
        if (questions.getSubject() == null) {
            return Optional.empty();
        }
        return Optional.of(questionsRepository.save(questions));
    }

    @Override
    public Optional<List<Questions>> saveQuestionsAll(List<Questions> questionsList) {
        return Optional.of(questionsRepository.saveAll(questionsList));
    }

    @Override
    public Questions saveQuestions(Questions questions) {
        return questionsRepository.save(questions);
    }

    @Override
    public Optional<Questions> updateQuestion(Questions questions) {
        return Optional.of(questionsRepository.save(questions));
    }

    @Override
    public void delQuestion(UUID id) {
        questionsRepository.deleteById(id);
    }
}
