package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Role;

import java.util.List;

public interface RoleService {

    public Role saveRole(Role role);
    public List<Role> getRole();
}
