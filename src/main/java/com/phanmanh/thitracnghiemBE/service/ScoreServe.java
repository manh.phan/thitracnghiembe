package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Score;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ScoreServe {
    Optional<List<Score>> getScore();
    Optional<Score> saveScore(Score score);
    Optional<Score> updateScore(Score score);
    void deleteScore(UUID id);
}
