package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Score;
import com.phanmanh.thitracnghiemBE.model.Subject;
import com.phanmanh.thitracnghiemBE.model.User;
import com.phanmanh.thitracnghiemBE.repository.ScoreRepository;
import com.phanmanh.thitracnghiemBE.repository.SubjectRepository;
import com.phanmanh.thitracnghiemBE.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service  @RequiredArgsConstructor
@Transactional
public class ScoreServeImp implements ScoreServe{
    private final ScoreRepository scoreRepository;
    private final UserRepository userRepository;
    private final SubjectRepository subjectRepository;
    @Override
    public Optional<List<Score>> getScore() {
        Score score1 = new Score();
        System.out.println(score1.toString());
        return Optional.of(scoreRepository.findAll());
    }

    @Override
    public Optional<Score> saveScore(Score score) {
        System.out.println(score);
        return Optional.of(scoreRepository.save(score));
    }

    @Override
    public Optional<Score> updateScore(Score score) {
        return Optional.of(scoreRepository.save(score));
    }

    @Override
    public void deleteScore(UUID id) {
        scoreRepository.deleteById(id);
    }
}
