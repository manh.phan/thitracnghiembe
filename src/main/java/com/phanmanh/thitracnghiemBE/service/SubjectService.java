package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Subject;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface SubjectService {

    Optional<Subject> getSubject(UUID id);
    List<Subject> getSubjects();
    Subject saveSubject(Subject subject);
    Optional<Subject> updateSubject(Subject subject);
    void delSubject(UUID id);
}
