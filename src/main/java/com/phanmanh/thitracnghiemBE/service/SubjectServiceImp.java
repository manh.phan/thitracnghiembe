package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Subject;
import com.phanmanh.thitracnghiemBE.repository.SubjectRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service @RequiredArgsConstructor
public class SubjectServiceImp implements SubjectService{
    private final SubjectRepository subjectRepository;

    @Override
    public Optional<Subject> getSubject(UUID id) {
        return subjectRepository.findById(id);
    }

    @Override
    public List<Subject> getSubjects() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject saveSubject(Subject subject) {
        return subjectRepository.save(subject);
    }

    @Override
    public Optional<Subject> updateSubject(Subject subject) {
        return Optional.of(subjectRepository.save(subject));
    }

    @Override
    public void delSubject(UUID id) {
        subjectRepository.deleteById(id);
    }
}
