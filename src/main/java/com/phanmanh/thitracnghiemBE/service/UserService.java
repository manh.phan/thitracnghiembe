package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService {
    Optional<User> saveUser(User user);
    Optional<User> updateUser(User user);
    void delUser(UUID id);
    Optional<User> getUser(UUID id);
    Optional<User> saveUserToRole(UUID userID, UUID roleID);
    List<User>getUsers();
}
