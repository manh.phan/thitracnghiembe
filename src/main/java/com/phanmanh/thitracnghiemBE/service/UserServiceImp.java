package com.phanmanh.thitracnghiemBE.service;

import com.phanmanh.thitracnghiemBE.model.Role;
import com.phanmanh.thitracnghiemBE.model.Subject;
import com.phanmanh.thitracnghiemBE.model.User;
import com.phanmanh.thitracnghiemBE.repository.RoleRepository;
import com.phanmanh.thitracnghiemBE.repository.SubjectRepository;
import com.phanmanh.thitracnghiemBE.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service @RequiredArgsConstructor @Transactional
public class UserServiceImp implements UserService{
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final SubjectRepository subjectRepository;

    @Override
    public Optional<User> saveUser(User user) {
        Optional<User> user1 = userRepository.findByUsername(user.getUsername());
        if (user1.isPresent() || user.getUsername() == null || user.getUsername().length() < 6 || user.getPassword().length() < 6)
            return Optional.empty();
        return Optional.of(userRepository.save(user));
    }

    @Override
    public Optional<User> saveUserToRole(UUID userID, UUID roleID) {
        Optional<User> user = userRepository.findById(userID);
        Optional<Role> role = roleRepository.findById(roleID);
        if (user.isPresent() && role.isPresent()) {
            user.get().setRole(role.get());
            return user;
        }
        return Optional.empty();
    }

    @Override
    public Optional<User> updateUser(User user) {
        return Optional.of(userRepository.save(user));
    }

    @Override
    public void delUser(UUID id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<User> getUser(UUID id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> getUsers() {
        return userRepository.findAll();
    }
}
